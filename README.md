# Notebooks #

This repository is for presenting supplementary material to research papers.

## Scope ##

- All scripts have been written by the owner of this repository for research conducted by the owner.
- The code in this repository is _not_ intended to be part of research submissions, but rather **exclusively for presentation purposes**.

## Presentations ##

See the [docs/*.ipynb](docs) files.

| Paper/Research | Notebook |
| :------------- | :------- |
| _Dilations of commuting $C\_{0}$-semigroups with bounded generators and the von Neumann polynomial inequality_ | [examples_dilations.ipynb](docs/examples_dilations.ipynb) |

**Note:** The Git provider _Gitea_ does not have a display format for ipynb-files.
To properly view and run the notebooks, users require python v3.10.x and the jupyter module.
To install the package requirements, call `just build`
or `python3 -m pip install -r requirements.txt`.#

## Usage ##

To run the presentations:

- Ensure you have a working copy of [**python 3.10.x**](https://www.python.org/downloads) on your system with rights to install packages.
- Clone the repository.
- Install the requirements. Navigate to the root of the repository and carry out:
  ```bash
  python3 -m pip install --disable-pip-version-check -r requirements.txt
  # for Windows users:
  py -3 -m pip install --disable-pip-version-check -r requirements.txt
  ```
- Start the desired notebooks using the [jupyter](https://jupyter.org) UI or run
  ```bash
  python3 -m jupyter notebook notebooks/xyz.ipynb
  # for Windows users:
  py -3 -m jupyter notebook notebooks/xyz.ipynb
  ```
  to run the notebook `docs/xyz.ipynb`.

Alternatively we recommend the following:

- Ensure you have a working copy of [**python 3.10.x**](https://www.python.org/downloads) on your system with rights to install packages.
- Ensure you have **bash** (for Windows users see the [Git SCM](https://gitforwindows.org))
  and the [**justfile**](https://github.com/casey/just#installation) tool
  (Windows users may wish to install [Chocolatey](https://chocolatey.org/install) beforehand).
- Clone the repository.
- Navigate in a bash terminal to the root of the repository and make use of the following commands:
  ```bash
  # for setup:
  just build
  # to run the notebook docs/xyz.ipynb:
  just run xyz
  ```
  (ensure that the extension is no included in the command!).
